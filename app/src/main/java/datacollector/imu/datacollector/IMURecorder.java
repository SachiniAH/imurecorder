package datacollector.imu.datacollector;

/**
 * Created by yanhang on 1/9/17.
 * Modified by Sachini
 */

import android.content.Intent;
import android.net.Uri;
import android.util.Log;

import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Calendar;
import java.util.Locale;
import java.util.Vector;

public class IMURecorder {
    private final static String LOG_TAG = IMURecorder.class.getName();

    MainActivity parent_;

    public static final int SENSOR_COUNT = 10;
    public static final int GYROSCOPE = 0;
    public static final int ACCELEROMETER = 1;
    public static final int MAGNETOMETER = 2;
    public static final int LINEAR_ACCELERATION = 3;
    public static final int GRAVITY = 4;
    public static final int ORIENTATION = 5;
    public static final int ROTATION = 6;
    public static final int STEP_COUNTER = 7;
    public static final int LOCATION = 8;
    public static final int PRESSURE = 9;

    private String output_dir;

    private BufferedWriter[] file_writers_ = new BufferedWriter[SENSOR_COUNT];
    //private Vector<Vector<String> > data_buffers_ = new Vector<>();
    private String[] default_file_names_ = {"gyro.txt", "acce.txt", "magnet.txt", "linacce.txt",
            "gravity.txt", "orientation.txt", "rotation.txt", "step.txt", "gps.txt", "pressure.txt"};

    public IMURecorder(String path, MainActivity parent, String user) {
        parent_ = parent;
        output_dir = path;
        Calendar file_timestamp = Calendar.getInstance();
        String header = "# Created at " + file_timestamp.getTime().toString() + " for user " + user + "\n";
        try {
            for (int i = 0; i < SENSOR_COUNT; ++i) {
                // Smaller buffer for location
                if (i == LOCATION) {
                    file_writers_[i] = createFile(path + "/" + default_file_names_[i], header, true);
                } else {
                    file_writers_[i] = createFile(path + "/" + default_file_names_[i], header, false);
                }
                //data_buffers_.add(new Vector<String>());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getOutputDir() {
        return output_dir;
    }

    private void writeBufferToFile(Writer writer, Vector<String> buffer) throws IOException {
        for (String line : buffer) {
            writer.write(line);
        }
        //writer.flush();
        writer.close();
    }

    public void endFiles() {
        try {
            for (int i = 0; i < SENSOR_COUNT; ++i) {
//                writeBufferToFile(file_writers_[i], data_buffers_.get(i));
                Log.i(LOG_TAG, "Closing files");
                file_writers_[i].flush();
                file_writers_[i].close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Boolean addRecord(long timestamp, float[] record, int kBins, int type) {
        if (type < 0 && type > SENSOR_COUNT) {
            Log.w(LOG_TAG, "Wrong Sensor type!");
            return false;
        }
        try {
            String line = String.format(Locale.US, "%d", timestamp);
            for (int i = 0; i < kBins; ++i) {
                line += String.format(Locale.US, " %.6f", record[i]);
            }
            line += "\n";
            file_writers_[type].write(line);
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public Boolean addIMURecord(long timestamp, float[] values, int type) {
        if (type < 0 && type >= SENSOR_COUNT) {
            Log.w(LOG_TAG, "Wrong Sensor type!");
            return false;
        }
        try {
            if (type == ORIENTATION) {
                //data_buffers_.get(type).add(String.format(Locale.US, "%d %.6f %.6f %.6f %.6f\n", timestamp, values[0], values[1], values[2], values[3]));
                file_writers_[type].write(String.format(Locale.US, "%d %.6f %.6f %.6f %.6f\n",
                        timestamp, values[0], values[1], values[2], values[3]));
            }else if (type == ROTATION) {
                //data_buffers_.get(type).add(String.format(Locale.US, "%d %.6f %.6f %.6f %.6f\n", timestamp, values[0], values[1], values[2], values[3]));
                file_writers_[type].write(String.format(Locale.US, "%d %.6f %.6f %.6f %.6f %.6f\n",
                        timestamp, values[0], values[1], values[2], values[3], values[4]));
            }else {
                //data_buffers_.get(type).add(String.format(Locale.US, "%d %.6f %.6f %.6f\n", timestamp, values[0], values[1], values[2]));
                file_writers_[type].write(String.format(Locale.US, "%d %.6f %.6f %.6f\n", timestamp, values[0], values[1], values[2]));
            }
            //file_writers_[type].flush();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private BufferedWriter createFile(String path, String header, boolean smaller) throws IOException {
        File file = new File(path);
        BufferedWriter writer;
        if (smaller) {
            writer = new BufferedWriter(new FileWriter(file), 256);
        } else {
            writer = new BufferedWriter(new FileWriter(file));
        }
        Intent scan_intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
        scan_intent.setData(Uri.fromFile(file));
        parent_.sendBroadcast(scan_intent);
        if (header != null && header.length() != 0) {
            writer.append(header);
            writer.flush();
        }
        return writer;
    }

    public Boolean addStepRecord(long timestamp, int value) {
        try {
            file_writers_[STEP_COUNTER].write(String.format(Locale.US, "%d %d\n", timestamp, value));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean addLocationRecord(long timestamp, long nanotime, float[] values, String provider) {
        try {
            file_writers_[LOCATION].write(String.format(Locale.US, "%d %.6f %.6f %.6f %.6f %s %d\n",
                    timestamp, values[0], values[1], values[2], values[3], provider, nanotime));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public Boolean addPressureRecord(long timestamp, float value) {
        try {
            file_writers_[PRESSURE].write(String.format(Locale.US, "%d %f\n", timestamp, value));
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }
}
