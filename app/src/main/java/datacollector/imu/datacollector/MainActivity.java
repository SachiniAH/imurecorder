package datacollector.imu.datacollector;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileNotFoundException;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;

public class MainActivity extends AppCompatActivity
        implements SensorEventListener {

    private Button mStartStopButton;

    private SensorManager mSensorManager;
    private Sensor mAccelerometer;
    private Sensor mGyroscope;
    private Sensor mGravity;
    private Sensor mLinearAcce;
    private Sensor mOrientation;    //Game Rotation
    private Sensor mMagnetometer;
    private Sensor mStepCounter;
    private Sensor mRotation;
    private Sensor mPressure;

    private float mInitialStepCount = -1;

    private LocationManager mLocationManager;

    private MyLocationListner mGPSListener;
    private MyLocationListner mNetworkListener;

    private IMURecorder mRecorder;
    private OutputDirectoryManager mOutputDirectoryManager;

    private AtomicBoolean mIsRecording = new AtomicBoolean(false);
    private AtomicBoolean gpsEnabled = new AtomicBoolean(false);

    // Gyroscope
    private TextView mLabelRx;
    private TextView mLabelRy;
    private TextView mLabelRz;
    // Accelerometer
    private TextView mLabelAx;
    private TextView mLabelAy;
    private TextView mLabelAz;
    // Linear acceleration
    private TextView mLabelLx;
    private TextView mLabelLy;
    private TextView mLabelLz;
    // Magnetometer
    private TextView mLabelMx;
    private TextView mLabelMy;
    private TextView mLabelMz;
    // Location
    private TextView mLabelGlat;
    private TextView mLabelGlong;
    private TextView mLabelGalt;
    private TextView mLabelSource;


    // @TODO tune
    private final long locationMinTime = 0;
    private final float locationMinDistance = 0;

    private PowerManager.WakeLock wl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        this.mStartStopButton = findViewById(R.id.startStopButton);

        this.mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        this.mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        this.mGyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
        this.mGravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
        this.mLinearAcce = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        this.mOrientation = mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);
        this.mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        this.mStepCounter = mSensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
        this.mRotation = mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR);
        this.mPressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);

        this.mLocationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);

        // initialize UI widgets
        mLabelRx = findViewById(R.id.label_rx);
        mLabelRy = findViewById(R.id.label_ry);
        mLabelRz = findViewById(R.id.label_rz);
        mLabelAx = findViewById(R.id.label_ax);
        mLabelAy = findViewById(R.id.label_ay);
        mLabelAz = findViewById(R.id.label_az);
        mLabelLx = findViewById(R.id.label_lx);
        mLabelLy = findViewById(R.id.label_ly);
        mLabelLz = findViewById(R.id.label_lz);
        mLabelMx = findViewById(R.id.label_mx);
        mLabelMy = findViewById(R.id.label_my);
        mLabelMz = findViewById(R.id.label_mz);
        mLabelGlat = findViewById(R.id.label_glat);
        mLabelGlong = findViewById(R.id.label_glong);
        mLabelGalt = findViewById(R.id.label_galt);
        mLabelSource = findViewById(R.id.label_source);

        // Request Permissions
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.WAKE_LOCK)
                != PackageManager.PERMISSION_GRANTED) {
            // No explanation needed, we can request the permission.
            showToast("Requesting permission");
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.WAKE_LOCK}, 1);
        }

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wl = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "My Tag");

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("Important")
                        .setCancelable(true)
                        .setMessage("Please disable Battery Optimization on Data Collector app. ")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        }).show();
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mIsRecording.get()) {
            stopRecording();
        }
        finishAndRemoveTask();
    }

    public void startStopRecording(View view) {
        if (!mIsRecording.get()) {
            EditText comment = findViewById(R.id.inputComment);
            String commentText = comment.getText().toString();
            if (commentText.isEmpty()) {
                showAlertAndStop("Enter valid name.");
                return;
            }
            try {
                if (startNewRecording(commentText)) {
                    mStartStopButton.setText(R.string.stop_title);
                }
            } catch (Exception e) {
                showToast(e.getMessage());
            }
        } else {
            stopRecording();
            mStartStopButton.setText(R.string.start_title);
        }
    }

    /* Start new recording. Return 1 if successful */
    private boolean startNewRecording(String comment) throws Exception {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            showAlertAndStop("Storage permission not granted");
            return false;
        }
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            showAlertAndStop("Location permission not granted");
            return false;
        }

        EditText prefix = findViewById(R.id.inputPrefix);
        String folderPrefix = "TC" + prefix.getText().toString();

        try {
            mOutputDirectoryManager = new OutputDirectoryManager(folderPrefix);
            mRecorder = new IMURecorder(mOutputDirectoryManager.getOutputDirectory(), this, comment);
        } catch (FileNotFoundException e) {
            new AlertDialog.Builder(MainActivity.this)
                    .setTitle(R.string.alert_title)
                    .setCancelable(false)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            stopRecording();
                        }
                    }).show();
            e.printStackTrace();
            return false;
        }

        mInitialStepCount = -1.0f;
        mIsRecording.set(true);

        this.mGPSListener = new MyLocationListner(LocationManager.GPS_PROVIDER);
        this.mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, locationMinTime, this.locationMinDistance, this.mGPSListener);
        this.mNetworkListener = new MyLocationListner(LocationManager.NETWORK_PROVIDER);
        this.mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, locationMinTime, this.locationMinDistance, this.mNetworkListener);

        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGyroscope, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mGravity, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mLinearAcce, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mOrientation, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mStepCounter, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mRotation, SensorManager.SENSOR_DELAY_FASTEST);
        mSensorManager.registerListener(this, mPressure, SensorManager.SENSOR_DELAY_FASTEST);

        this.wl.acquire();

        return true;
    }

    private void stopRecording() {
        mIsRecording.set(false);
        this.mLocationManager.removeUpdates(this.mGPSListener);
        this.mLocationManager.removeUpdates(this.mNetworkListener);
        this.mGPSListener = null;
        this.mNetworkListener = null;

        if (mRecorder != null) {
            mRecorder.endFiles();
        }

        mSensorManager.unregisterListener(this, mAccelerometer);
        mSensorManager.unregisterListener(this, mGyroscope);
        mSensorManager.unregisterListener(this, mGravity);
        mSensorManager.unregisterListener(this, mLinearAcce);
        mSensorManager.unregisterListener(this, mOrientation);
        mSensorManager.unregisterListener(this, mMagnetometer);
        mSensorManager.unregisterListener(this, mStepCounter);
        mSensorManager.unregisterListener(this, mRotation);
        mSensorManager.unregisterListener(this, mPressure);
        showToast("Stopped");

        this.wl.release();
    }

    private void showAlertAndStop(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                new AlertDialog.Builder(MainActivity.this)
                        .setTitle("@string/alert_title")
                        .setMessage(text)
                        .setCancelable(false)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                stopRecording();
                            }
                        }).show();
            }
        });
    }

    private void showToast(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onSensorChanged(final SensorEvent event) {
        long timestamp = event.timestamp;
        float[] values = {0.0f, 0.0f, 0.0f, 0.0f, 0.0f};
        // final Boolean mIsWriteFile = mConfig.getFileEnabled();

        switch (event.sensor.getType()) {
            case Sensor.TYPE_ACCELEROMETER:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLabelAx.setText(String.format(Locale.US, "%.6f", event.values[0]));
                        mLabelAy.setText(String.format(Locale.US, "%.6f", event.values[1]));
                        mLabelAz.setText(String.format(Locale.US, "%.6f", event.values[2]));
                    }
                });
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 3);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.ACCELEROMETER);
                }
                break;
            case Sensor.TYPE_GYROSCOPE:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLabelRx.setText(String.format(Locale.US, "%.6f", event.values[0]));
                        mLabelRy.setText(String.format(Locale.US, "%.6f", event.values[1]));
                        mLabelRz.setText(String.format(Locale.US, "%.6f", event.values[2]));
                    }
                });
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 3);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.GYROSCOPE);
                }
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLabelLx.setText(String.format(Locale.US, "%.6f", event.values[0]));
                        mLabelLy.setText(String.format(Locale.US, "%.6f", event.values[1]));
                        mLabelLz.setText(String.format(Locale.US, "%.6f", event.values[2]));
                    }
                });
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 3);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.LINEAR_ACCELERATION);
                }
                break;
            case Sensor.TYPE_GRAVITY:
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 3);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.GRAVITY);
                }
                break;
            case Sensor.TYPE_GAME_ROTATION_VECTOR:
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 4);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.ORIENTATION);
                }
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mLabelMx.setText(String.format(Locale.US, "%.6f", event.values[0]));
                        mLabelMy.setText(String.format(Locale.US, "%.6f", event.values[1]));
                        mLabelMz.setText(String.format(Locale.US, "%.6f", event.values[2]));
                    }
                });
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 3);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.MAGNETOMETER);
                }
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                if (mIsRecording.get()) {
                    System.arraycopy(event.values, 0, values, 0, 5);
                    mRecorder.addIMURecord(timestamp, values, IMURecorder.ROTATION);
                }
                break;
            case Sensor.TYPE_PRESSURE:
                if (mIsRecording.get()) {
                    mRecorder.addPressureRecord(timestamp, event.values[0]);
                }
                break;
            case Sensor.TYPE_STEP_COUNTER:
                if (mIsRecording.get()) {
                    if (mInitialStepCount < 0) {
                        mInitialStepCount = event.values[0] - 1;
                    }
                    if (mIsRecording.get()) {
                        mRecorder.addStepRecord(timestamp, (int) (event.values[0] - mInitialStepCount));
                    }
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            int cur_step = (int) (event.values[0] - mInitialStepCount);
                            // mLabelStepCount.setText(String.valueOf(cur_step));
                        }
                    });
                }
                break;
        }
    }

    public void testStatus(View view) {
        if (mIsRecording.get()) {
            showToast("Still running!");
        } else {
            showToast("It has stopped!");
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    public class MyLocationListner implements LocationListener {
        Location mLastLocation;

        MyLocationListner(String provider) {
            showToast("LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(final Location location) {
            mLastLocation = location;
            long timestamp = location.getTime();
            long nanotime = location.getElapsedRealtimeNanos();
            // Latitude, Longitude, altitude, accuracy, provider
            float[] values = {0.0f, 0.0f, 0.0f, 0.0f};

            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    mLabelGlat.setText(String.format(Locale.US, "%.6f", location.getLatitude()));
                    mLabelGlong.setText(String.format(Locale.US, "%.6f", location.getLongitude()));
                    mLabelGalt.setText(String.format(Locale.US, "%.6f", location.getAltitude()));
                    mLabelSource.setText(location.getProvider());
                }
            });
            if (mIsRecording.get()) {
                values[0] = (float) location.getLatitude();
                values[1] = (float) location.getLongitude();
                values[2] = (float) location.getAltitude();
                values[3] = location.getAccuracy();
                mRecorder.addLocationRecord(timestamp, nanotime, values, location.getProvider());
            }

        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {
            showToast("Gps is turned on!! ");
        }

        @Override
        public void onProviderDisabled(String s) {
            showToast("Gps is turned off!! ");
            Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
            startActivity(intent);
        }
    }

    public void closeApp(View view) {
        this.onDestroy();
    }

}
